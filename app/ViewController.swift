//
//  ViewController.swift
//  CheckIMEI
//
//  Created by Thais Vitorasso Matos on 14/09/15.
//  Copyright (c) 2015 Thais Vitorasso Matos. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var consultaButton: UISearchBar!
    
    var consultaIMEI:NSDictionary!
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar){
        consultaButton.resignFirstResponder()
        var jsonResult: NSDictionary!
        let JSONObject: [String : AnyObject] = [
            "IMEI" : consultaButton.text
        ]
       
        if NSJSONSerialization.isValidJSONObject(JSONObject) {
            var request: NSMutableURLRequest = NSMutableURLRequest()
            let url = "http://api.checkimei.com.br/api/check_imei"
            
            var err: NSError?
            
            request.URL = NSURL(string: url)
            request.HTTPMethod = "POST"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.HTTPBody = NSJSONSerialization.dataWithJSONObject(JSONObject, options:  NSJSONWritingOptions(rawValue:0), error: &err)
            
            var response: NSURLResponse?
            let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &err)
            
            var jsonResult = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
            
            let resultadoPage = self.storyboard?.instantiateViewControllerWithIdentifier("ResultadoViewController") as! ResultadoViewController
            
            resultadoPage.imei = jsonResult["imei"] as! String
            resultadoPage.status = jsonResult["status"] as! String
            
            consultaIMEI=jsonResult
            
            performSegueWithIdentifier("consultaIMEI", sender:self)
            /*
            let resultadoPageNav = UINavigationController (rootViewController: resultadoPage)
            
            let appDelagate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            appDelagate.window?.restorationIdentifier = "ViewController"
            
            appDelagate.window?.rootViewController = resultadoPageNav
            */
            
            
            /*
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) {(response, data, error) -> Void in
                if error != nil {
                    println("error")
                    
                } else {
                    jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary

                    let resultadoPage = self.storyboard?.instantiateViewControllerWithIdentifier("ResultadoViewController") as! ResultadoViewController
                    
                    resultadoPage.imei = jsonResult["imei"] as! String
                    resultadoPage.status = jsonResult["status"] as! String
                    
                    let resultadoPageNav = UINavigationController (rootViewController: resultadoPage)
                    
                    let appDelagate = UIApplication.sharedApplication().delegate as! AppDelegate
                    
                    appDelagate.window?.rootViewController = resultadoPageNav
                    
                    println("AsSynchronous\(jsonResult)")
                }
            }
        */
        }
        
    }
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        consultaButton.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "consultaIMEI"{
        var svc = segue.destinationViewController as! ResultadoViewController;
        svc.imei = consultaIMEI["imei"] as! String
        svc.status = consultaIMEI["status"] as! String
        
        }
    }
}

