//
//  ConsultarViewController.swift
//  checkimei
//
//  Created by thiago on 9/21/15.
//  Copyright (c) 2015 Thais Vitorasso Matos. All rights reserved.
//

import UIKit

class ConsultarViewController: UIViewController {

    @IBOutlet weak var imeiTxtField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        if (segue.identifier == "consultar") {
            var jsonResult: NSDictionary!
            let JSONObject: [String : AnyObject] = [
                "IMEI" : imeiTxtField.text
            ]
            
            if NSJSONSerialization.isValidJSONObject(JSONObject) {
                var request: NSMutableURLRequest = NSMutableURLRequest()
                let url = "http://api.checkimei.com.br/api/check_imei"
                
                var err: NSError?
                
                request.URL = NSURL(string: url)
                request.HTTPMethod = "POST"
                request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.HTTPBody = NSJSONSerialization.dataWithJSONObject(JSONObject, options:  NSJSONWritingOptions(rawValue:0), error: &err)
                
                var response: NSURLResponse?
                let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &err)
                
                var jsonResult = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
                
                var resultadoPage = segue.destinationViewController as! ResultadoViewController;
                
                resultadoPage.imei = jsonResult["imei"] as! String
                resultadoPage.status = jsonResult["status"] as! String
            }
            
        }
    }
}
