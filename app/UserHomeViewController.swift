//
//  UserHomeViewController.swift
//  checkimei
//
//  Created by Thais Vitorasso Matos on 15/09/15.
//  Copyright (c) 2015 Thais Vitorasso Matos. All rights reserved.
//

import UIKit

class UserHomeViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableDevices: UITableView!
    //@IBOutlet weak var labelTeste: UILabel!
    //var token:String!
    let textCellIdentifier = "TextCell"
    var userToken: String!
    private var count:Int = 0
    private var max:Int!
    var devices:[NSDictionary]!
    var base64pass: String!
    var device:NSDictionary!
    var editDeviceViewController:EditDeviceViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var error: NSError?
        var text:String = ""
        //println(self.userToken)
        println(self.base64pass)
        let PasswordString = "\(self.userToken)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        let urlPath: String = "http://api.checkimei.com.br/api/get_devices"
        var url: NSURL = NSURL(string: urlPath)!
        var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.setValue("Basic \(self.base64pass)", forHTTPHeaderField: "Authorization")
        request.HTTPMethod = "GET"
        var jsonResult: NSDictionary!
        
        var response: NSURLResponse?
        
        let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &error)
        
        if (error != nil){
            let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Erro na requisição dos smartphones", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            println(error)
            return
        }
        //println(data)
        var reply = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary

        if ( reply["devices"] !== NSNull()){
            self.devices = reply["devices"] as! [NSDictionary]
        }
        else {
            self.devices = [NSDictionary]()
        }
        
        tableDevices.dataSource = self
        tableDevices.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        //max = devices.count
        println(userToken)
        tableDevices.dataSource = self
        tableDevices.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITextFieldDelegate Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableDevices.dequeueReusableCellWithIdentifier(textCellIdentifier, forIndexPath: indexPath) as! UITableViewCell
        
        let row = indexPath.row
        if devices.count > row {
            cell.textLabel?.text = (devices[row]["apelido"] as! String)
        } else {
            cell.textLabel?.text = "Cadastrar novo Smartphone"
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate Methods
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let row = indexPath.row
        if row < devices.count {
            device = devices[row]
            editDeviceViewController.device_id = device["device_id"] as! String
            editDeviceViewController.imei = device["imei"] as! String
            editDeviceViewController.modelo = device["modelo"] as! String
            editDeviceViewController.apelido = device["apelido"] as! String
            editDeviceViewController.marca = device["marca"] as! String
        } else {
            //editDeviceViewController.device_id = UIDevice.currentDevice().identifierForVendor!.UUIDString
            editDeviceViewController.device_id = String(arc4random_uniform(999999999))
            editDeviceViewController.imei = ""
            editDeviceViewController.modelo = ""
            editDeviceViewController.apelido = ""
            editDeviceViewController.marca = ""
        }
        
        editDeviceViewController.base64pass = base64pass
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "editDevice"{
            editDeviceViewController = segue.destinationViewController as! EditDeviceViewController
            /*
            editDeviceViewController.imei = device["imei"] as! String
            editDeviceViewController.status = device["imei"] as! String
            editDeviceViewController.imei = device["imei"] as! String
            editDeviceViewController.imei = device["imei"] as! String
            */
            
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}
