//
//  LoginViewController.swift
//  checkimei
//
//  Created by Thais Vitorasso Matos on 15/09/15.
//  Copyright (c) 2015 Thais Vitorasso Matos. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usuarioTxt: UITextField!
    @IBOutlet weak var senhaTxt: UITextField!
    
    var email: String!
    var token: String!
    var base64pass: String!
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        let PasswordString = "\(usuarioTxt.text):\(senhaTxt.text)"
        
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        let urlPath: String = "http://api.checkimei.com.br/api/token"
        var url: NSURL = NSURL(string: urlPath)!
        var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.HTTPMethod = "GET"
        var jsonResult: NSDictionary!
        
        var response: NSURLResponse?
        var error: NSError?
        let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &error)
        
        if (error != nil){
            let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Login Inválido", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            return false
        }
        
        var reply = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary

        self.token = reply["token"] as! String
        self.base64pass = base64EncodedCredential
        
        return true
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        if (segue.identifier == "loginSegue") {
            var svc = segue.destinationViewController as! HomeTabBarControllerViewController;
            svc.token = self.token
            svc.base64pass = self.base64pass
            //println("\(LoginViewController.jsonResult)")
            //svc.token = reply["token"] as! String
        }
        
        /*if let httpResponse = response as? NSHTTPURLResponse {
            println(httpResponse)
            if (segue.identifier == "loginSegue") {
                var svc = segue.destinationViewController as! UserHomeViewController;
                //println("\(LoginViewController.jsonResult)")
                svc.token = "132456"
            }
        }*/
        
        /*var task = NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var err: NSError
            jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
        })*/
        
        
        
        
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usuarioTxt.text = email
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
