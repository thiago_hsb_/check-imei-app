//
//  RegisterViewController.swift
//  checkimei
//
//  Created by thiago on 9/27/15.
//  Copyright (c) 2015 Thais Vitorasso Matos. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var sobrenomeTextField: UITextField!
    @IBOutlet weak var sexoTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var senha2TextField: UITextField!
    
    var emailOk: Bool = false
    var senhaOk: Bool = false
    let dateFormat: NSDateFormatter = NSDateFormatter()
    let datePicker: UIDatePicker = UIDatePicker()
    
    let pickOption = ["Masculino", "Feminino", "Outro"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormat.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormat.dateFormat = "dd-MM-yyyy"
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: Selector("updateDateField:"), forControlEvents:UIControlEvents.ValueChanged)
        dateTextField.inputView = datePicker
        
        var pickerView = UIPickerView()
        
        pickerView.delegate = self
        
        sexoTextField.inputView = pickerView
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func endEmailEditing(sender: UITextField) {
        var jsonResult: NSDictionary!
        let JSONObject: [String : AnyObject] = [
            "email" : emailTextField.text
        ]
        
        if NSJSONSerialization.isValidJSONObject(JSONObject) {
            var request: NSMutableURLRequest = NSMutableURLRequest()
            let url = "http://api.checkimei.com.br/api/check_email_exists"
            
            var err: NSError?
            
            request.URL = NSURL(string: url)
            request.HTTPMethod = "POST"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.HTTPBody = NSJSONSerialization.dataWithJSONObject(JSONObject, options:  NSJSONWritingOptions(rawValue:0), error: &err)
            
            var response: NSURLResponse?
            let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &err)
            
            if (err != nil){
                let alert: UIAlertView = UIAlertView(title: "ERRO", message: "Erro inesperado", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                return
            }
            
            var jsonResult = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
            
            if jsonResult["email_exists"] as! Int == 1{
                let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Email já existe", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                emailOk = false
                emailTextField.textColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
            } else {
                if isValidEmail(emailTextField.text){
                    emailTextField.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
                    emailOk = true
                } else {
                    let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Email inválido", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    emailOk = false
                    emailTextField.textColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
                }                
            }
        }
    }
    
    @IBAction func confirmarSenhaEditing(sender: UITextField) {
        
        if senhaTextField.text == senha2TextField.text {
            senha2TextField.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
            senhaOk = true
            return
        }
        
        senha2TextField.textColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Senhas não batem", delegate: self, cancelButtonTitle: "OK")
        alert.show()
        senhaOk = false
    }
    
    @IBAction func confirmarSenhaChanged(sender: UITextField) {
        if senhaTextField.text == senha2TextField.text {
            senha2TextField.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
            senhaOk = true
            return
        }
        
        senha2TextField.textColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        //let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Senhas não batem", delegate: self, cancelButtonTitle: "OK")
        //alert.show()
        senhaOk = false
    }
    
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        
        if !emailOk{
            let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Email já existe", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            return false
        }
        
        if nomeTextField.text == nil || sobrenomeTextField.text == nil || sexoTextField.text == nil || dateTextField.text == nil || emailTextField == nil || senha2TextField.text == nil || senhaTextField.text == nil {
            
            let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Favor preencher todas as informações", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            return false
        }
        
        if !senhaOk{
            let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Senhas não batem", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            return false
        }
        
        var jsonResult: NSDictionary!
        let JSONObject: [String : AnyObject] = [
            "email": emailTextField.text,
            "password": senhaTextField.text,
            "name": nomeTextField.text,
            "surname": sobrenomeTextField.text,
            "gender": sexoTextField.text,
            "birth_date": dateTextField.text
        ]
        
        if NSJSONSerialization.isValidJSONObject(JSONObject) {
            var request: NSMutableURLRequest = NSMutableURLRequest()
            let url = "http://api.checkimei.com.br/api/users"
            
            var err: NSError?
            
            request.URL = NSURL(string: url)
            request.HTTPMethod = "POST"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.HTTPBody = NSJSONSerialization.dataWithJSONObject(JSONObject, options:  NSJSONWritingOptions(rawValue:0), error: &err)
            
            var response: NSURLResponse?
            let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &err)
            
            if (err != nil){
                let alert: UIAlertView = UIAlertView(title: "ERRO", message: "Erro inesperado", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                return false
            }
            
            
        }
        
        
        return true
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    //picker
    
    func updateDateField(sender: UIDatePicker) {
        dateTextField.text = dateFormat.stringFromDate(sender.date)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return pickOption[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sexoTextField.text = pickOption[row]
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "cadastroSegue") {
            var svc = segue.destinationViewController as! LoginViewController;
            svc.email = emailTextField.text
            //println("\(LoginViewController.jsonResult)")
            //svc.token = reply["token"] as! String
        }

    }
    

}
