//
//  EditDeviceViewController.swift
//  app
//
//  Created by Thiago Henrique on 10/6/15.
//  Copyright (c) 2015 Thiago Henrique. All rights reserved.
//

import UIKit

class EditDeviceViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var apelidoTxtField: UITextField!
    @IBOutlet weak var imeiTxtField: UITextField!
    @IBOutlet weak var marcaTxtField: UITextField!
    @IBOutlet weak var modeloTxtField: UITextField!
    @IBOutlet weak var statusTxtField: UITextField!
    
    var device_id: String!
    var apelido: String!
    var imei: String!
    var marca: String!
    var modelo: String!
    var status: String!
    var cadastro: Bool = false
    var base64pass: String!
    
    let pickOption = ["OK", "Roubado", "Furtado", "Perdido"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var pickerView = UIPickerView()
        
        pickerView.delegate = self
        
        statusTxtField.inputView = pickerView
        
        apelidoTxtField.text = apelido
        imeiTxtField.text = imei
        marcaTxtField.text = marca
        modeloTxtField.text = modelo
        statusTxtField.text = status
        
        if imei == ""{
            cadastro = true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func salvarButtonClick(sender: UIButton) {
        
        if apelidoTxtField.text != "" && imeiTxtField.text != "" && marcaTxtField.text != "" && modeloTxtField.text != "" {
            
            if cadastro {
                var jsonResult: NSDictionary!
                let JSONObject: [String : AnyObject] = [
                    "device_id": device_id,
                    "imei": imeiTxtField.text,
                    "apelido": apelidoTxtField.text,
                    "modelo": modeloTxtField.text,
                    "marca": marcaTxtField.text
                ]
            
                if NSJSONSerialization.isValidJSONObject(JSONObject) {
                    var request: NSMutableURLRequest = NSMutableURLRequest()
                    let url = "http://api.checkimei.com.br/api/cadastro_device"
                
                    var err: NSError?
                
                    request.URL = NSURL(string: url)
                    request.HTTPMethod = "POST"
                    request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("Basic \(base64pass)", forHTTPHeaderField: "Authorization")
                    request.HTTPBody = NSJSONSerialization.dataWithJSONObject(JSONObject, options:  NSJSONWritingOptions(rawValue:0), error: &err)
                
                    var response: NSURLResponse?
                    let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &err)
                
                    if (err != nil){
                        let alert: UIAlertView = UIAlertView(title: "ERRO", message: "Erro inesperado", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                    }
                
                }

                let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Smartphone Cadastrado com Sucesso", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
            else {
                var jsonResult: NSDictionary!
                let JSONObject: [String : AnyObject] = [
                    "device_id": device_id,
                    "imei": imeiTxtField.text,
                    "apelido": apelidoTxtField.text,
                    "modelo": modeloTxtField.text,
                    "marca": marcaTxtField.text,
                    "status": statusTxtField.text,
                    "infos": ""
                ]
                
                if NSJSONSerialization.isValidJSONObject(JSONObject) {
                    var request: NSMutableURLRequest = NSMutableURLRequest()
                    let url = "http://api.checkimei.com.br/api/update_device"
                    
                    var err: NSError?
                    
                    request.URL = NSURL(string: url)
                    request.HTTPMethod = "POST"
                    request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("Basic \(base64pass)", forHTTPHeaderField: "Authorization")
                    request.HTTPBody = NSJSONSerialization.dataWithJSONObject(JSONObject, options:  NSJSONWritingOptions(rawValue:0), error: &err)
                    
                    var response: NSURLResponse?
                    let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &err)
                    
                    if (err != nil){
                        let alert: UIAlertView = UIAlertView(title: "ERRO", message: "Erro inesperado", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                    }
                    
                }
                
                let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Smartphone Alterado com Sucesso", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
        }
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return pickOption[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        statusTxtField.text = pickOption[row]
    }

}
