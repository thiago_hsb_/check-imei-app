//
//  SettingsViewController.swift
//  
//
//  Created by Thiago on 11/22/15.
//
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var new_password: UITextField!
    @IBOutlet weak var confirm_password: UITextField!
    var base64pass: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func alterarClick(sender: AnyObject) {
        let JSONObject: [String : AnyObject] = [
            "password" : password.text,
            "new_password": new_password.text,
            "confirm_password": confirm_password.text
        ]
        
        var error: NSError?
        var text:String = ""
        //println(self.userToken)
        let urlPath: String = "http://api.checkimei.com.br/api/update_password"
        var url: NSURL = NSURL(string: urlPath)!
        var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(JSONObject, options:  NSJSONWritingOptions(rawValue:0), error: &error)
        request.setValue("Basic \(self.base64pass)", forHTTPHeaderField: "Authorization")
        request.HTTPMethod = "POST"
        
        
        var jsonResult: NSDictionary!
        
        var response: NSURLResponse?
        
        let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &error)
        
        if (error != nil){
            let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Erro na requisição dos smartphones", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            println(error)
            return
        }
        
        //println(data)
        //var reply = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
        let alert: UIAlertView = UIAlertView(title: "Aviso", message: "Senha Alterada com Sucesso", delegate: self, cancelButtonTitle: "OK")
        alert.show()

        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
