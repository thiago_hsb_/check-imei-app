//
//  ResultadoViewController.swift
//  checkimei
//
//  Created by Usuário Convidado on 14/09/15.
//  Copyright (c) 2015 Thais Vitorasso Matos. All rights reserved.
//

import UIKit


class ResultadoViewController: UIViewController {

    @IBOutlet weak var resultadoImagem: UIImageView!
    @IBOutlet weak var imeiNumber: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    var imei: String!
    var status: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        imeiNumber.text = imei
        statusLabel.text = status
                
        if status == "Roubado" || status == "Furtado" {
            resultadoImagem.image = UIImage(named: "img_roubado")
        } else if status == "OK" {
            resultadoImagem.image = UIImage(named: "img_ok")
        } else if status == "Perdido" {
            resultadoImagem.image = UIImage(named: "img_perdido")
        } else {
            resultadoImagem.image = UIImage(named: "img_ok")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
