//
//  HomeTabBarControllerViewController.swift
//  checkimei
//
//  Created by Thiago on 9/19/15.
//  Copyright (c) 2015 Thais Vitorasso Matos. All rights reserved.
//

import UIKit

class HomeTabBarControllerViewController: UITabBarController , UITabBarControllerDelegate {

    var token: String!
    var base64pass: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var userHome = self.viewControllers![0] as! UserHomeViewController
        userHome.userToken = token

        userHome.base64pass = self.base64pass
        
        var settings = self.viewControllers![2] as! SettingsViewController
        settings.base64pass = self.base64pass
        
        //userHome.devices = ["123","456", "789"]
        self.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        var userHome = self.viewControllers![0] as! UserHomeViewController
        //userHome.userToken = token
        //userHome.devices = ["123","456", "789"]       //logView.log.append("Testing 123")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
